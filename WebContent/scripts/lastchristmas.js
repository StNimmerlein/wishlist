'use strict;'

var btn;

function addClick() {
	btn.addEventListener('click', displayMessage);
}

function getRandom(max) {
    return Math.random() * max;
}

function moveButton() {
	btn.style.left = getRandom(1000) + 'px';
	btn.style.top = getRandom(500) + 'px';
}

function displayMessage() {
	alert('Sehr clever. Dumm nur, das der Button eigentlich nichts tut.');
}

function addMouseover() {
	btn.addEventListener('mouseover', moveButton);
}

function startMusic() {
	createButton();
	createMusic();
	addMouseover();
	addClick();
}

function createButton() {
	var fm = document.createElement('form');
	btn = document.createElement('input');
	btn.type = 'button';
	btn.value = 'Musik beenden';
	btn.style.position = 'absolute';
	btn.style.left = '500px';
	btn.style.top = '200px';
	fm.appendChild(btn);
	document.body.appendChild(fm);
}

function createMusic() {
	var fme = document.createElement('iframe');
	fme.width = 0;
	fme.height = 0;
	fme.src = 'https://www.youtube.com/embed/FahD-Sy4Jhg?autoplay=1';
	document.body.appendChild(fme);
}

document.addEventListener('DOMContentLoaded', startMusic);