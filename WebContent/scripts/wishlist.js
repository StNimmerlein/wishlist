'use strict;'
var wishes = ['Puppenhaus', 'Nussknacker', 'Spielzeugeisenbahn', 'Spielzeugsoldat', 'Haus', 'Teuren Schmuck', 'Schokolade', 'Kekse', 'Weltfrieden', 'Apache Kampfhelikopter'];
var table;

var initialPresentX = 800;
var initialPresentY = 430;
var currentPresentX = initialPresentX;
var currentPresentY = initialPresentY;
var currentPresentIndex = 0;
var presentHeight = 75;
var presentYOffset = 40;
var presentXOffset = 50;
var presentStackHeight = 3;

function createWishPage() {
	createTable();
	addTree();
}

function addTree() {
	var tree = document.createElement('img');
	tree.src = 'WebContent/img/tree_medium.png';
	tree.style.position = 'absolute';
	tree.style.left = '800px';
	tree.style.top = '75px';
	tree.height = 450;
	document.body.appendChild(tree);
}

function createTable() {
	table = document.createElement('table');
	var row = table.insertRow(0);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	cell1.innerHTML = '<b>Besorgt</b>';
	cell2.innerHTML = '<b>Wunsch</b>';
	table.style.marginLeft = 100;
	table.style.marginTop = 100;
	document.body.appendChild(table);
}

function colorTable() {
	table.style.color = table.style.color == 'red' ? 'blue' : 'red';
}

function computeCheckbox() {
	if (this.checked) {
		addPresent();
	} else {
		removePresent();
	}
}

function increasePresentPositionAndIndex() {
	currentPresentIndex++;
	if (currentPresentIndex % 3 == 0) {
		currentPresentY = initialPresentY;
		currentPresentX += presentXOffset;
	} else {
		currentPresentY -= presentYOffset;
	}
}

function decreasePresentPositionAndIndex() {
	if (currentPresentIndex % 3 == 0) {
		currentPresentY = initialPresentY - (presentStackHeight-1)*presentYOffset;
		currentPresentX -= presentXOffset;
	} else {
		currentPresentY += presentYOffset;
	}
	currentPresentIndex--;
}

function addPresent() {
	var present = document.createElement('img');
	present.style.position = 'absolute';
	present.style.top = currentPresentY;
	present.style.left = currentPresentX;
	present.id = 'present' + currentPresentIndex;
	present.src = 'WebContent/img/present_small.png';
	present.height = presentHeight;
	document.body.appendChild(present);
	
	increasePresentPositionAndIndex();
}

function removePresent() {
	var present = document.getElementById('present' + (currentPresentIndex-1));
	present.parentNode.removeChild(present);
	decreasePresentPositionAndIndex();
}

function buildWishlist() {
	var wishlength = wishes.length;
	for (var i = 0; i < wishlength; i++) {
		var row = table.insertRow(1);
		var wishCheckboxCell = row.insertCell(0);
		var wishTextCell = row.insertCell(1);
		wishTextCell.innerHTML = wishes[i];
		
		var wishForm = document.createElement('form');
		wishCheckboxCell.appendChild(wishForm);
		var wishCheckBox = document.createElement('input');
		wishCheckBox.type = 'checkbox';
		wishCheckBox.addEventListener('change', computeCheckbox);
		wishForm.appendChild(wishCheckBox);
	}
}

function startWishlist() {
	createWishPage();
	buildWishlist();
	setInterval(colorTable, 100);
}

document.addEventListener('DOMContentLoaded', startWishlist);