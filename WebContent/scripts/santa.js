'use strict;'

function getRandom(max) {
    return Math.random() * max;
}

function Santa() {
	this.image = 'WebContent/img/santa.png';
	this.startLeft = '800px';
	this.startTop = '200px';
	this.width = 200;
	this.maxSpeed = 10;
	this.maxLeft = window.innerWidth;
	this.maxTop = window.innerHeight;
	this.xSpeed = 4;
	this.ySpeed = 4;
	this.animationDelay = 100;
	this.santa;
	this.speedRandomizer;
}

Santa.prototype.createSanta = function() {
	var santa = document.createElement('img');
	santa.src = this.image;
	santa.style.position = 'absolute';
	santa.style.left = this.startLeft;
	santa.style.top = this.startTop;
	santa.width = this.width;
	document.body.appendChild(santa);
	return santa;
}

Santa.prototype.directSanta = function() {
	this.santa.style.transitionDuration = "0.5s"
	this.directX();
	this.directY();
}

Santa.prototype.directX = function() {
	if (this.xSpeed > 0) {
		this.santa.style.transform = "rotatey(180deg)";
	} else {
		this.santa.style.transform = "rotatey(0deg)";
	}
}

Santa.prototype.directY = function() {
	if (this.ySpeed > 0) {
		this.santa.style.transform = this.santa.style.transform + " rotate(0deg)";
	} else {
		this.santa.style.transform = this.santa.style.transform + " rotate(45deg)";
	}
}

Santa.prototype.beginAnimation = function() {
	this.moveY();
	this.moveX();
	var _this = this;
	setTimeout(function() {_this.beginAnimation();} , this.animationDelay);
}

Santa.prototype.moveX = function() {
	var santaX = parseInt(this.santa.style.left, 10);
	if ((santaX >= 0) && (santaX <= this.maxLeft)) {
		this.santa.style.left = Math.min(Math.max((santaX + this.xSpeed), 0), this.maxLeft) + 'px';
	}
}

Santa.prototype.moveY = function() {
	var santaY = parseInt(this.santa.style.top, 10);
	if ((santaY >= 0) && (santaY <= this.maxTop)) {
		this.santa.style.top = Math.min(Math.max((santaY + this.ySpeed), 0), this.maxTop) + 'px';
	}
}

Santa.prototype.beginSpeedRandomization = function() {
	var maxRand = this.maxSpeed*2 + 1;
	this.xSpeed = getRandom(maxRand) - this.maxSpeed;
	this.ySpeed = getRandom(maxRand) - this.maxSpeed;
	var timeUntilNextSpeedChange = getRandom(1000) + 1000;
	this.directSanta();
	var _this = this;
	setTimeout(function() {_this.beginSpeedRandomization();}, timeUntilNextSpeedChange);
}

Santa.prototype.instanciate = function() {
	this.santa = this.createSanta();
	this.beginSpeedRandomization();
	this.beginAnimation();
}



function buildSanta() {
	document.body.style.overflow = 'hidden';
	var santa = new Santa();
	santa.instanciate();
}

document.addEventListener('DOMContentLoaded', buildSanta);