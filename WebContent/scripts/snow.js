'use strict;'

function SnowFlake(xPos, size) {
	this.flake;
	this.ySpeed = 2;
	this.size = size;
	this.xSpeed = 0;
	this.animationSpeed = 50;
	this.xPos = xPos;
	this.yPos = -size;
	this.yMax = window.innerHeight;
	this.maxXSpeed = this.ySpeed;
	this.wind = 100;
	this.speedRandomizer;
	this.animation;
}

SnowFlake.prototype.create = function() {
	var image = document.createElement('img');
	image.src = 'WebContent/img/snow_tiny.png';
	image.width = this.size;
	image.style.position = 'absolute';
	image.style.left = this.xPos;
	image.style.top = this.yPos;
	document.body.appendChild(image);
	return image;
}

SnowFlake.prototype.instanciate = function() {
	this.flake = this.create();
	this.beginXRandomization();
	this.startFalling();
}

SnowFlake.prototype.remove = function() {
	this.flake.parentNode.removeChild(this.flake);
	clearTimeout(this.speedRandomizer);
	clearTimeout(this.animation);
}

SnowFlake.prototype.startFalling = function() {
	if (this.yPos > this.yMax) {
		this.remove();
		return;
	}
	
	this.moveY();
	this.moveX();
	var _this = this;
	this.animation = setTimeout(function() {_this.startFalling();}, this.animationSpeed);
}

SnowFlake.prototype.moveY = function() {
	this.yPos += this.ySpeed;
	this.flake.style.top = this.yPos;
}

SnowFlake.prototype.moveX = function() {
	this.xPos += this.xSpeed;
	this.flake.style.left = this.xPos;
}

SnowFlake.prototype.beginXRandomization = function() {
	this.xSpeed = this.xSpeed + (getRandom(2) - 1);
	this.xSpeed = Math.min(this.xSpeed, this.maxXSpeed);
	this.xSpeed = Math.max(this.xSpeed, (-1)*this.maxXSpeed);
	
	var _this = this;
	this.speedRandomizer = setTimeout(function() {_this.beginXRandomization();}, this.snow);
}

function getRandom(max) {
    return Math.random() * max;
}

function SnowStorm(delay) {
	this.delay = delay;
	this.minSize = 5;
	this.maxSize = 20;
	this.maxX = window.innerWidth;
}

SnowStorm.prototype.instanciate = function() {
	var xPos = getRandom(this.maxX);
	var size = getRandom(this.maxSize-this.minSize) + this.minSize;
	var flake = new SnowFlake(xPos, size);
	flake.instanciate();
	
	var _this = this;
	setTimeout(function() {_this.instanciate();}, this.delay);
}

function letItSnow() {
	var snow = new SnowStorm(50);
	snow.instanciate();
}

document.addEventListener('DOMContentLoaded', letItSnow);